#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


# In[2]:


data = pd.read_csv("data.csv")
#data = data.iloc[1:,:]
data['low'] = data['low'].astype(float)
data['high'] = data['high'].astype(float)
data['average'] = data[['low', 'high']].mean(axis=1)
dataset = data[['average','volume','open']]

split1 = int(0.8 * dataset.shape[0])
split2= int(.2*dataset.shape[0])
training_data = dataset[:split1]
testing_data = dataset[split1:]
real_stock=testing_data[['open']]
dataset = data[['average','volume']]
training_data = dataset[:split1]
testing_data = dataset[split1:]
print(training_data.shape)


# In[3]:


# 30 20
from sklearn.preprocessing import MinMaxScaler
def Vol(training_set_scaled):
    X_train=[]  
    Y_train=[]
    X1_train=[]
    for i in range(0,len(training_set_scaled)):
        val=training_set_scaled[i][1]
        X1_train.append(val)
    X1_train=np.array(X1_train)
    return X1_train
def VOL_AVG(time_step,X1_train,training_set_scaled):
    X1=[]
    Y=[]
    X2=[]
    for i in range(time_step,len(training_set_scaled)):
    
        X1.append(training_set_scaled[i-time_step:i,0])
        X2.append(X1_train[i-time_step:i,0])
        Y.append(training_set_scaled[i,0])
    return X1,X2,Y
def global_lis1(X_train,X2_train):
    lis1=[]
    lis=[]
    
    for i in range(len(X_train)):
        a=X_train[i]
        b=X2_train[i]
        lis.append(a)
        lis.append(b)
        lis1.append(lis)
        lis=[]
    return lis1
def Recurrent_Neural_Network(cell_count, time_step, training_set, layer):

    xyz=data[['open']]
    sc1=MinMaxScaler(feature_range = (0,1))
    tra=sc1.fit_transform(xyz)
   
    
    sc = MinMaxScaler(feature_range = (0, 1))
    training_set_scaled = sc.fit_transform(training_data)
  
    X_train=[]  
    Y_train=[]
    X1_train=[]
    X2_train=[]
 
    X1_train=Vol(training_set_scaled)
    X1_train=np.array(X1_train)

    X1_train=X1_train.reshape(len(X1_train),1)
   
    X_train,X2_train,Y_train=VOL_AVG(time_step,X1_train,training_set_scaled)
    X_train=np.array(X_train)
    Y_train=np.array(Y_train)
    X2_train=np.array(X2_train)
    global_lis=[]
    global_lis=global_lis1(X_train,X2_train)
   
    X_train=np.array(global_lis)
    print(X_train.shape)
        
    X_train, Y_train = np.array(X_train), np.array(Y_train)
  

    # Reshaping
    X_train = np.reshape(X_train, (X_train.shape[0], time_step, 2))
   

    
    # Initialising the RNN
    regressor = Sequential()

    # Adding the first LSTM layer and some Dropout regularisation
    regressor.add(LSTM(units = cell_count, return_sequences = True, input_shape = (X_train.shape[1], 2)))
    regressor.add(Dropout(0.2))

    # Adding a second LSTM layer and some Dropout regularisation
    if layer > 2:
        regressor.add(LSTM(units = cell_count, return_sequences = True))
        regressor.add(Dropout(0.2))

    # Adding a fourth LSTM layer and some Dropout regularisation
    regressor.add(LSTM(units = cell_count))
    regressor.add(Dropout(0.2))

    # Adding the output layer
    regressor.add(Dense(units = 1))
    
    # Compiling the RNN
    regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')
    
    
    

    # Fitting the RNN to the Training set
    regressor.fit(X_train, Y_train, epochs = 3, batch_size = 32)
    
    
 
    
    dataset_total = pd.concat((training_data, testing_data), axis = 0)
    inputs = dataset_total[len(dataset_total) - len(testing_data) - time_step:].values
    for i in range(len(inputs)):
         inputs[i][0]=float(inputs[i][0])
  
    print("mai shape ",inputs.shape)
    #inputs = inputs.reshape(-1 ,1)
    testing_set_scaled=sc.fit_transform(inputs)
    print("saled dim",testing_set_scaled.shape)
    X_train=[]
    Y_train=[]
    X1_train=[]
    X2_train=[]
    
    X1_train=Vol(testing_set_scaled)
    X1_train=np.array(X1_train)

    X1_train=X1_train.reshape(len(X1_train),1)
    
       
    X_train,X2_train,Y_train=VOL_AVG(time_step,X1_train,testing_set_scaled)
    X_train=np.array(X_train)
    
    X2_train=np.array(X2_train)
    global_lis=[]
    lis=[]
    global_lis=global_lis1(X_train,X2_train)
    X_train=np.array(global_lis)
    print(X_train.shape)
    # Visualising the results
    X_train = np.reshape(X_train, (X_train.shape[0], time_step, 2))
    predicted_stock_price = regressor.predict(X_train)
    predicted_stock_price = sc1.inverse_transform(predicted_stock_price)
    plt.plot(real_stock, color = 'red', label = 'Real Google Stock Price')
    plt.plot(predicted_stock_price, color = 'blue', label = 'Predicted Google Stock Price')
    plt.title('Google Stock Price Prediction')
    plt.xlabel('Time')
    plt.ylabel('Google Stock Price')
    plt.legend()
    plt.show()


# In[ ]:



for layer in [2,3]:
    for cells in [30,50,80]:
        for time in [20,50,75]:
            print("--------------------------------------------------------------------------------")
            print("--------------------------------------------------------------------------------")
            print(layer,cells,time)
            print("--------------------------------------------------------------------------------")
            Recurrent_Neural_Network(cells, time, training_data, layer)


# In[ ]:




